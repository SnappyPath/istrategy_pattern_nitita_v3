﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonExample : MonoBehaviour
{
    public static SingletonExample Instance;
    void Start()
    {
        if (Instance!=null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    internal void DoSomething()
    {
        throw new NotImplementedException();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
