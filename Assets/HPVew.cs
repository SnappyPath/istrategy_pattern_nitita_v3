﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPVew : MonoBehaviour
{
    [SerializeField]
    public TMPro.TextMeshProUGUI HPText;

    [SerializeField]
    public Hero hero;
    void Start()
    {
        hero.onHpChanged += UpdateView;
    }

    private void UpdateView(int v)
    {
        HPText.text = v.ToString();
    }

    void Update()
    {
        
    }
}
