﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class MoveForward : iStrategy
{
    public int movementspeed;
    public MoveForward(int integer)
    {
        movementspeed = integer;
    }

    public void Perform(Transform transform)
    {
        transform.Translate(Vector3.forward * movementspeed * Time.deltaTime);
    }
}
