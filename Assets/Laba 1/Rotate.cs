﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : iStrategy
{
    public int rotatespeed;
    public Rotate(int integer)
    {
        rotatespeed = integer;
    }
    public void Perform(Transform transform)
    {
        transform.Rotate(Vector3.up * rotatespeed * Time.deltaTime);
    }
}
