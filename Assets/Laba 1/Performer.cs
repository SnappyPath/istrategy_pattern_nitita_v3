﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performer : MonoBehaviour
{
    private iStrategy CurrentStrategy;
    void Start()
    {
        
    }

    void Update()
    {
        CurrentStrategy?.Perform(transform);
    }

    public void SetStrategy(iStrategy strat)
    {
        CurrentStrategy = strat;
    }

    public void SetStrategyByInt(int integer)
    {
        if (integer == 1)
        {
            SetStrategy(new MoveForward(5));
        }
        if (integer == 2)
        {
            SetStrategy(new Rotate(100));
        }
        if (integer == 3)
        {
            SetStrategy(new Emmit(1));
        }
    }
}
