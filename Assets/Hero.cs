﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
    public int HP = 100;

    public Action<int> onHpChanged = (v) => { };
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            ChangeHP(10);
        }
    }

    public void ChangeHP(int val)
    {
        HP -= val;
        onHpChanged(HP);
    }
}
